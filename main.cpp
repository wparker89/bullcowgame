//
//  main.cpp
//  BullCowGame
//
//  Created by Will Parker on 21/03/2017.
//  Copyright © 2017 Will Parker. All rights reserved.
//

/* This is the console executable, that makes use of the BullCow class
 This acts as the view in an MVC pattern, and is responsible for all 
 user interaction.  For game logic, see the FBullCowGame class
 */

#include <iostream>
#include <string>
#include "FBullCowGame.hpp"

// To make syntax unreal friendly
using FText = std::string;
using int32 = int;

bool AskToPlayAgain();
FText GetValidGuess();
void PlayGame();
void PrintIntro();
void PrintGameSummary();

FBullCowGame BCGame;

int main() {
    
    do
    {
        PlayGame();
    }
    while (AskToPlayAgain());
    
    return 0;
}

bool AskToPlayAgain()
{
    std::cout << std::endl << "Would you like to play again? (y/n)" << std::endl;
    FText response = "";
    getline(std::cin, response);
    std::cout << std::endl;
    return (response[0] == 'y') || (response[0] == 'Y');
}

FText GetValidGuess()
{
    EGuessStatus guessStatus = EGuessStatus::Invalid_Status;
    FText guess = "";
    
    do
    {
        std::cout << "Try " << BCGame.GetCurrentTry() << " of " << BCGame.GetMaxTries() << ": Enter your' guess" << std::endl;
        std::cout << "Please enter a guess: " << std::endl;
        getline(std::cin, guess);
        std::cout << std::endl;
        guessStatus = BCGame.CheckGuessValidity(guess);
        
        switch (guessStatus)
        {
            case EGuessStatus::Incorrect_Length:
                std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word." << std::endl << std::endl;
                break;
            case EGuessStatus::Not_Isogram:
                std::cout << "Please enter an isogram." << std::endl << std::endl;
                break;
            case EGuessStatus::Not_Lowercase:
                std::cout << "Please only enter lowercase letters." << std::endl << std::endl;
                break;
            default:
                guessStatus = EGuessStatus::Ok;
        }
    } while (guessStatus != EGuessStatus::Ok);
    
    return guess;
}

void PlayGame()
{
    BCGame.Reset();
    PrintIntro();
    int32 maxTries = BCGame.GetMaxTries();
    
    while (!BCGame.IsGameWon() && BCGame.GetCurrentTry() <= maxTries)
    {
        FText guess = GetValidGuess();
        FBullCowCount bullCowCount = BCGame.SubmitValidGuess(guess);
        std::cout << "Number of Bulls: " << bullCowCount.Bulls;
        std::cout << ", Number of Cows: " << bullCowCount.Cows << std::endl;
        std::cout << "You guessed: " << guess << std::endl << std::endl;
    }
    PrintGameSummary();
}

void PrintIntro()
{
    std::cout << "Welcome to Bulls and Cows, a fun word game." << std::endl;
    std::cout << "Can you guess the " << BCGame.GetHiddenWordLength() << " letter isogram I am thinking of?" << std::endl << std::endl;
}

void PrintGameSummary()
{
    if (BCGame.IsGameWon())
    {
        std::cout << std::endl;
        std::cout << "Congratualtions, you won!  It took you ";
        // Try gets incremented straight after the guess is entered, so 2 here technically gives us the first.
        if (BCGame.GetCurrentTry() == 2)
        {
            std::cout << BCGame.GetCurrentTry() - 1 << " try ";
        }
        else
        {
            std::cout << BCGame.GetCurrentTry() - 1 << " tries ";
        }
        std::cout << "to win!" << std::endl << std::endl;
    }
    else
    {
        std::cout << "Sorry, you lost!  The correct answer was " << BCGame.GetHiddenWord() << ".  Better luck next time!";
    }
}
