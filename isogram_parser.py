'''
Determines whether a word is an isogram or not.
'''
def is_isogram(word):
	for letter in word:
		if word.count(letter) > 1:
			return False
	return True


# Quick script to build a list of isograms from the SOWPODS word list used in scrabble.  
# Uniqueness of words in the SOWPODS list has been assumed.
isogram_list = []
file = open('SOWPODS.txt', 'r')
for word in file:
	if (is_isogram(word)):
		isogram_list.append(word)
file.close()

# Quick script to format and add tricky isograms sourced online
'''with open('hard-isograms.txt', 'r') as file:
	lines = file.readlines()
for line in lines:
	words = line.split()
	for word in words:
		word = word.lower()
		if not word in isogram_list:
			isogram_list.append(word)'''

# Finally create isogram file
file = open('isograms.txt', 'w')
for word in isogram_list:
	file.write(word)
file.close()