//
//  FBullCowGame.cpp
//  BullCowGame
//
//  Created by Will Parker on 21/03/2017.
//  Copyright © 2017 Will Parker. All rights reserved.
//

#include "FBullCowGame.hpp"
#include <map>
#include <iostream>
#define TMap std::map

// To make syntax unreal friendly
using int32 = int;

// Getters

EGuessStatus FBullCowGame::CheckGuessValidity(FString guess) const
{
    if (!IsIsogram(guess))
    {
        return EGuessStatus::Not_Isogram;
    }
    else if (!isLowerCase(guess))
    {
        return EGuessStatus::Not_Lowercase;
    }
    else if (guess.length() != GetHiddenWordLength())
    {
        return EGuessStatus::Incorrect_Length;
    }
    else
    {
        return EGuessStatus::Ok;
    }
}
int32 FBullCowGame::GetMaxTries() const
{
    TMap<int32, int32> wordLengthToMaxTries {
        {3, 4}, {4, 7}, {5, 10}, {6, 16}, {7, 20}, {8, 25}, {9, 31}, {10, 37}, {11, 45}, {12, 54}, {13, 60}, {14, 70}, {15, 80}, {16, 100,}, {17, 101}
    };
    return wordLengthToMaxTries[(int32)myHiddenWord.length()];
}
int32 FBullCowGame::GetCurrentTry() const { return myCurrentTry; }
int32 FBullCowGame::GetHiddenWordLength() const { return (int32)myHiddenWord.length(); }
bool FBullCowGame::IsGameWon() const { return isGuessCorrect; }
bool FBullCowGame::IsIsogram(FString word) const
{
    // treat zero and one letter words as isograms
    if (word.length() <= 1)
    {
        return true;
    }
    TMap<char, bool> letterSeen;
    
    for (auto letter : word)
    {
        letter = tolower(letter);
        if (letterSeen[letter])
        {
            return false;
        }
        else
        {
            letterSeen[letter] = true;
        }
    }
    return true;
}

bool FBullCowGame::isLowerCase(FString word) const
{
    for (auto letter : word)
    {
        if (!islower(letter))
        {
            return false;
        }
    }
    return true;
}

// Constructors

FBullCowGame::FBullCowGame()
{
    Reset();
}

// Member functions
void FBullCowGame::Reset()
{
    setHiddenWord();
    
    myCurrentTry = 1;
    
    isGuessCorrect = false;
}

FBullCowCount FBullCowGame::SubmitValidGuess(FString guess)
{
    myCurrentTry++;
    FBullCowCount bullCowCount;
    
    int32 hiddenWordLength = (int32)myHiddenWord.length();
    for (int32 i = 0; i < hiddenWordLength; i++)
    {
        for (int32 j = 0; j < hiddenWordLength; j++)
        {
            if (guess[i] == myHiddenWord[j])
            {
                if (i == j)
                {
                    bullCowCount.Bulls++;
                }
                else
                {
                    bullCowCount.Cows++;
                }
            }
        }
    }
    if (bullCowCount.Bulls == hiddenWordLength)
    {
        isGuessCorrect = true;
    }
    return bullCowCount;
}

int FBullCowGame::getNumberOfLines(const FString filepath)
{
    // Need to first loop through and count the number of lines.
    int32 numberOfLines = 0;
    std::ifstream isogram_list(filepath);
    if (isogram_list.is_open())
    {
        while (std::getline(isogram_list, myHiddenWord))
        {
            numberOfLines++;
        }
    }
    isogram_list.close();
    return numberOfLines;
}

int FBullCowGame::getRandomLineNumber(int numberOfLines)
{
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 eng(rd()); // seed the generator
    std::uniform_int_distribution<> distr(1, numberOfLines); // define the range
    int32 lineNumber = distr(eng);
    return lineNumber;
}

void FBullCowGame::setHiddenWord()
{
    const FString ISOGRAM_LIST = "/Path/To/isogram_list.txt";
    int numberOfLines = getNumberOfLines(ISOGRAM_LIST);
    int lineNumber = getRandomLineNumber(numberOfLines);
    
    // Finally loop through again to set the hidden word to the randomly selected line number.
    std::ifstream isogram_list2(ISOGRAM_LIST);
    for (int32 i = 0; i < lineNumber; i++)
    {
        getline(isogram_list2, myHiddenWord);
    }
    isogram_list2.close();
}

FString FBullCowGame::GetHiddenWord() const
{
    return myHiddenWord;
}
