# README #

A game where a player attempts to guess isograms that are randomly selected from the SOWPODS scrabble dictionary. 

### What is this repository for? ###

* This game is an implementation of the Bull Cow game as presented in this [Unreal course](https://www.udemy.com/unrealcourse/) on Udemy. It contains several modifcations from the initial suggested game.

### How do I get set up? ###

* The only change after cloning the respository is to edit the ISOGRAM_PATH string found in the class file FBullCowGame.cpp. This needs to be edited to point to where you have chosen to keep the file.

### Who do I talk to? ###

* Repo owner or admin