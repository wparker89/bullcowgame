//
//  FBullCowGame.hpp
//  BullCowGame
//
//  Created by Will Parker on 21/03/2017.
//  Copyright © 2017 Will Parker. All rights reserved.
//

#ifndef FBullCowGame_hpp
#define FBullCowGame_hpp

#include <string>
#include <fstream>
#include <random>

// To make syntax unreal friendly
using FString = std::string;
using int32 = int;

struct FBullCowCount
{
    int32 Bulls = 0;
    int32 Cows = 0;
};

enum class EGuessStatus
{
    Invalid_Status,
    Ok,
    Not_Isogram,
    Incorrect_Length,
    Not_Lowercase
};

class FBullCowGame
{
public:
    
    // Constructors
    FBullCowGame();
    
    // Getters
    EGuessStatus CheckGuessValidity(FString) const;
    int32 GetMaxTries() const;
    int32 GetCurrentTry() const;
    int32 GetHiddenWordLength() const;
    FString GetHiddenWord() const; 
    bool IsGameWon() const;
    
    // Member functions
    void Reset();
    FBullCowCount SubmitValidGuess(FString);
    
private:
    // Initialised in constructor
    int32 myCurrentTry;
    FString myHiddenWord;
    bool isGuessCorrect;
    
    bool IsIsogram(FString) const;
    bool isLowerCase(FString) const;
    
    void setHiddenWord();
    int getNumberOfLines(const FString);
    int getRandomLineNumber(int);
};

#endif /* FBullCowGame_hpp */
